<?php
use CRM_Bij1migratie_ExtensionUtil as E;

/**
 * Class BIJ1 migratie service
 *
 * @author Erik Hommel (CiviCooP) <erik.hommel@civicoop.org>
 * @date 9 Dec 021
 * @license AGPL-3.0
 */
class CRM_Bij1migratie_Bij1MigratieService {
  /**
   * @var CRM_Bij1migratie_Bij1MigratieService
   */
  protected static $singleton;
  protected $_mijnheerAanhef = NULL;
  protected $_mevrouwAanhef = NULL;
  protected $_manGeslacht = NULL;
  protected $_onbekendGeslacht = NULL;
  protected $_vrouwGeslacht = NULL;
  protected $_financialTypeId = NULL;
  protected $_completedContributionStatusId = NULL;
  protected $_pendingContributionStatusId = NULL;
  protected $_eftPaymentId = NULL;
  protected $_rcurPaymentId = NULL;
  protected $_frstPaymentId = NULL;
  protected $_ooffPaymentId = NULL;
  protected $_migratieGroupId = NULL;

  /**
   * CRM_Bij1migratie_Bij1MigratieService constructor.
   */
  public function __construct() {
    if (!self::$singleton) {
      self::$singleton = $this;
    }
  }

  /**
   * @return \CRM_Bij1migratie_Bij1MigratieService
   */
  public static function getInstance() {
    if (!self::$singleton) {
      self::$singleton = new CRM_Bij1migratie_Bij1MigratieService();
    }
    return self::$singleton;
  }

  /**
   * @param int $id
   */
  public function setMigratieGroepId(int $id) {
    $this->_migratieGroupId = $id;
  }

  /**
   * @return null
   */
  public function getMigratieGroepId() {
    return $this->_migratieGroupId;
  }

  /**
   * @param int $value
   */
  public function setMijnheerAanhef(int $value) {
    $this->_mijnheerAanhef = $value;
  }

  /**
   * @return null
   */
  public function getMijnheerAanhef() {
    return $this->_mijnheerAanhef;
  }

  /**
   * @param int $value
   */
  public function setMevrouwAanhef(int $value) {
    $this->_mevrouwAanhef = $value;
  }

  /**
   * @return null
   */
  public function getMevrouwAanhef() {
    return $this->_mevrouwAanhef;
  }

  /**
   * @param int $value
   */
  public function setManGeslacht(int $value) {
    $this->_manGeslacht = $value;
  }

  /**
   * @return null
   */
  public function getManGeslacht() {
    return $this->_manGeslacht;
  }

  /**
   * @param int $value
   */
  public function setOnbekendGeslacht(int $value) {
    $this->_onbekendGeslacht = $value;
  }

  /**
   * @return null
   */
  public function getOnbekendGeslacht() {
    return $this->_onbekendGeslacht;
  }

  /**
   * @param int $value
   */
  public function setVrouwGeslacht(int $value) {
    $this->_vrouwGeslacht = $value;
  }

  /**
   * @return null
   */
  public function getVrouwGeslacht() {
    return $this->_vrouwGeslacht;
  }

  /**
   * @param int $id
   */
  public function setFinancialTypeId(int $id) {
    $this->_financialTypeId = $id;
  }

  /**
   * @return null
   */
  public function getFinancialTypeId() {
    return $this->_financialTypeId;
  }

  /**
   * @param int $id
   */
  public function setCompletedContributionStatusId(int $id) {
    $this->_completedContributionStatusId = $id;
  }

  /**
   * @return null
   */
  public function getCompletedContributionStatusId() {
    return $this->_completedContributionStatusId;
  }

  /**
   * @param int $id
   */
  public function setPendingContributionStatusId(int $id) {
    $this->_pendingContributionStatusId = $id;
  }

  /**
   * @return null
   */
  public function getPendingContributionStatusId() {
    return $this->_pendingContributionStatusId;
  }

  /**
   * @param int $id
   */
  public function setEftPaymentId(int $id) {
    $this->_eftPaymentId = $id;
  }

  /**
   * @return null
   */
  public function getEftPaymentId() {
    return $this->_eftPaymentId;
  }

  /**
   * @param int $id
   */
  public function setFrstPaymentId(int $id) {
    $this->_frstPaymentId = $id;
  }

  /**
   * @return null
   */
  public function getFrstPaymentId() {
    return $this->_frstPaymentId;
  }

  /**
   * @param int $id
   */
  public function setOoffPaymentId(int $id) {
    $this->_ooffPaymentId = $id;
  }

  /**
   * @return null
   */
  public function getOoffPaymentId() {
    return $this->_ooffPaymentId;
  }

  /**
   * @param int $id
   */
  public function setRcurPaymentId(int $id) {
    $this->_rcurPaymentId = $id;
  }

  /**
   * @return null
   */
  public function getRcurPaymentId() {
    return $this->_rcurPaymentId;
  }

  /**
   * Function om mogelijke separatoren voor csv bestand op te halen
   *
   * @return string[]
   */
  public function getSeparatorList() {
    return [",", ";", ":", "~"];
  }

  /**
   * Method om de separator voor het csv bestand te halen
   *
   * @param $separatorId
   * @return string
   */
  public function getSeparator($separatorId) {
    switch ($separatorId) {
      case 0:
        return ",";
      case 1:
        return ";";
      case 3:
        return ":";
      case 2:
        return "~";
      default:
        return "";
    }
  }

  /**
   * Method om formValues te schonen
   *
   * @param $sourceValues
   * @return array
   */
  public function cleanFormValues($sourceValues) {
    $result = [];
    foreach ($sourceValues as $sourceKey => $sourceValue) {
      if ($sourceKey != "qfKey" && $sourceKey != "entryURL" && substr($sourceKey,0,4) != "_qf_") {
        $result[$sourceKey] = $sourceValue;
      }
    }
    return $result;
  }

  /**
   * Method om de migratie kolommen op te halen
   *
   * @param string $migratieType
   * @return false|array
   * @throws
   */
  public function getMigratieColumns(string $migratieType) {
    switch ($migratieType) {
      case "A":
        $type = "airtable";
        break;
      case "B":
        $type = "betalingen";
        break;
      default:
        $type = "contacten";
        break;
    }
    $container = CRM_Extension_System::singleton()->getFullContainer();
    $jsonFile = $container->getPath('bij1migratie').'/mapping/' . strtolower($type) . '.json';
    if (file_exists($jsonFile)) {
      return json_decode(file_get_contents($jsonFile));
    }
    return FALSE;
  }

  /**
   * Omzetten land naar country id voor airtable
   *
   * @param $airtableLand
   * @return int|void
   */
  public function haalAirtableLand($airtableLand) {
    $airtableLand = strtolower($airtableLand);
    switch ($airtableLand) {
      case "belgie":
      case "belgië":
        $isoCode = "BE";
        break;
      case "brazilië":
        $isoCode = "BR";
        break;
      case "canada":
        $isoCode = "CA";
        break;
      case "curacao":
      case "curaçao":
        $isoCode = "CW";
        break;
      case "denemarken":
        $isoCode = "DK";
        break;
      case "duitsland":
      case "deutschland":
        $isoCode = "DE";
        break;
      case "engeland":
      case "verenigd koninkrijk":
        $isoCode = "GB";
        break;
      case "finland":
        $isoCode = "FI";
        break;
      case "frankrijk":
        $isoCode = "FR";
        break;
      case "griekenland":
        $isoCode = "GR";
        break;
      case "ierland":
        $isoCode = "IE";
        break;
      case "italië":
      case "italie":
        $isoCode = "IT";
        break;
      case "kenia":
        $isoCode = "KE";
        break;
      case "kuwait":
        $isoCode = "KW";
        break;
      case "mexico":
        $isoCode = "MX";
        break;
      case "oostenrijk":
        $isoCode = "AT";
        break;
      case "polen":
        $isoCode = "PL";
        break;
      case "portugal":
        $isoCode = "PT";
        break;
      case "spanje":
        $isoCode = "ES";
        break;
      case "verenigde staten":
        $isoCode = "US";
        break;
      case "zweden":
        $isoCode = "SE";
        break;
      case "zwitserland":
        $isoCode = "CH";
        break;
      case "suriname":
        $isoCode = "SR";
        break;
      default:
        $isoCode = "NL";
        break;
    }
    $landId = CRM_Core_DAO::singleValueQuery("SELECT id FROM civicrm_country WHERE iso_code = %1", [1 => [$isoCode, "String"]]);
    if ($landId) {
      return (int) $landId;
    }
  }

  /**
   * Method om betalingsmethode te vertalen
   *
   * @param string $betalingsinstrument
   * @return null
   */
  public function haalBetalingsinstrument(string $betalingsinstrument) {
    switch ($betalingsinstrument) {
      case "first":
        return $this->_frstPaymentId;
        break;
      case "oneoff":
        return $this->_ooffPaymentId;
        break;
      case "recurring":
        return $this->_rcurPaymentId;
        break;
      default:
        return $this->_eftPaymentId;
        break;
    }
  }

}
