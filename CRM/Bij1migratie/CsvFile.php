<?php
use CRM_Bij1migratie_ExtensionUtil as E;

/**
 * Class voor BIJ1 migratie CSV import
 *
 * @author Erik Hommel (CiviCooP) <erik.hommel@civicoop.org>
 * @date 9 dec 2021
 * @license AGPL-3.0
 */
class CRM_Bij1migratie_CsvFile {

  private $_separator = NULL;
  private $_fileName = NULL;
  private $_usesHeaders = NULL;
  private $_rowNumber = NULL;
  private $_headers = [];
  private $_migratieType = NULL;
  public $csv = NULL;

  /**
   * Method to initialize
   *
   * @param string $fileName
   * @param string $migratieType
   * @param bool $usesHeaders
   * @param string $separator
   */
  public function initialize(string $fileName, string $migratieType, bool $usesHeaders = FALSE, string $separator = "~") {
    $this->_fileName = $fileName;
    $this->_separator = $separator;
    $this->_usesHeaders = $usesHeaders;
    $this->_rowNumber = 0;
    $this->_migratieType = $migratieType;
  }
  /**
   * Method to get the headers (and numbers if none found)
   *
   * @throws Exception if file can not be opened
   */
  public function getHeaders() {
    $headers = [];
    $i = 0;
    $this->open();
    if ($this->_usesHeaders) {
      $data = fgetcsv($this->csv, 0, $this->_separator);
      foreach ($data as $key => $value) {
        if ($value) {
          $headers[] = $value;
          $i++;
        }
        else {
          $i++;
          $headers[] = "csv column " . $i;
        }
      }
    }
    else {
      $data = fgetcsv($this->csv, 0, $this->_separator);
      $count = count($data);
      while ($i < $count) {
        $i++;
        $headers[] = "csv column " . $i;
      }
    }
    $this->close();
    return $headers;
  }

  /**
   * Method to open the file for read
   *
   * @throws Exception when file can not be opened
   * @return bool
   */
  public function open() {
    $this->csv = fopen($this->_fileName, 'r');
    if ($this->csv) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Method to read the next line and return an array with data
   *
   * @param string $tableName
   * @return array|bool
   */
  public function readNext(string $tableName) {
    $this->_rowNumber++;
    // if row 1 complete mapping and if headers, skip
    if ($this->_rowNumber == 1) {
      $data = fgetcsv($this->csv, 0, $this->_separator);
      $this->setHeaders($data);
      if (!$this->_usesHeaders) {
        rewind($this->csv);
      }
    }
    $data = fgetcsv($this->csv, 0, $this->_separator);
    if ($tableName && $data) {
      $this->sanitizeData($tableName, $data);
    }
    if ($data) {
      return $data;
    }
    return FALSE;
  }

  /**
   * Sanitize row: empty data if it contains invalid content
   *
   * @param string $tableName
   * @param array $data
   * @throws
   */
  private function sanitizeData(string $tableName, array &$data) {
    if (!empty($data)) {
      $table = new CRM_Bij1migratie_ImportTable();
      $columns = $table->getColumnNames($tableName);
      foreach ($columns as $key => $name) {
        switch ($this->_migratieType) {
          case "A":
            $this->sanitizeAirtable($name, $data);
            break;
          case "B":
            $this->sanitizeBetalingen($name, $key, $data);
            break;
          case "P":
            $this->sanitizePersonen($name, $data);
            break;
        }
      }
      foreach ($data as $key => $value) {
        if (is_numeric($key)) {
          unset($data[$key]);
        }
      }
    }
  }

  /**
   * Method om airtable data op te schonen voor form processor verwerking
   *
   * @param string $name
   * @param array $data
   * @throws Exception
   */
  private function sanitizeAirtable(string $name, array &$data) {
    switch ($name) {
      case "lidstatus":
        $data[$name] = strtolower($data[1]);
        break;
      case "voornaam":
        $data[$name] = $data[2];
        break;
      case "achternaam":
        $data[$name] = $data[3];
        break;
      case "email":
        $data[$name] = $data[4];
        break;
      case "straat_huisnummer":
        $data[$name] = $data[5];
        break;
      case "postcode":
        $data[$name] = $data[6];
        break;
      case "plaats":
        $data[$name] = $data[7];
        break;
      case "land":
        $landId = Civi::service('bij1Migratie')->haalAirtableLand($data[8]);
        if ($landId) {
          $data[$name] = $landId;
        }
        else {
          $data[$name] = 1152;
        }
        break;
      case "inschrijvingsdatum":
        if (!empty($data[9])) {
          $data[9] = str_replace("/", "-", $data[9]);
          $inschrijvingsdatum = new DateTime($data[9]);
          $data[$name] = $inschrijvingsdatum->format("d-m-Y");
        }
        break;
      case "lidnummer":
        if (is_numeric($data[0])) {
          $data[$name] = $data[0];
        }
        break;
      case "notitie":
        $data[$name] = $data[10];
        break;
      case "telefoon":
        $data[$name] = $data[11];
        break;
      case "mollie_customer_id":
        $data[$name] = $data[13];
        break;
      case "iban":
        $data[$name] = $data[12];
        break;
    }
  }

  /**
   * Method om data voor betalingen samen te stellen
   *
   * @param string $name
   * @param string $key
   * @param array $data
   * @throws
   */
  private function sanitizeBetalingen(string $name, string $key, array &$data) {
    switch ($name) {
      case "transactie_id":
        $data[$name] = $data[0];
        break;
      case "aanmaakdatum":
        if (!empty($data[1])) {
          $donatieDatum = new DateTime($data[1]);
          $data[$name] = $donatieDatum->format('d-m-Y');
        }
        break;
      case "betalingsdatum":
        if (!empty($data[2])) {
          $betalingsDatum = new DateTime($data[2]);
          $data[$name] = $betalingsDatum->format('d-m-Y');
        }
        break;
      case "omschrijving":
        if (!empty($data[3])) {
          $data[$name] = $data[3];
        }
        else {
          $data[$name] = "Migratie CiviCRM";
        }
        break;
      case "betalingsmethode":
        if (!empty($data[4])) {
          $data[$name] = $data[4];
        }
        break;
      case "status":
        $data[$name] = $data[5];
        break;
      case "betalingsinstrument":
        if (!empty($data[6])) {
          $data[$name] = Civi::service('bij1Migratie')->haalBetalingsinstrument($data[6]);
        }
        break;
      case "mollie_customer_id":
        $data[$name] = $data[7];
        break;
      case "mandaat":
        $data[$name] = $data[8];
        break;
      case "bedrag":
        $data[$name] = $data[9];
        break;
      case "munteenheid":
        $data[$name] = $data[10];
        break;
      case "klant":
        $data[$name] = $data[11];
        break;
      case "email":
        $data[$name] = $data[12];
        break;
    }
  }

  /**
   * Method om data voor personen samen te stellen
   *
   * @param string $name
   * @param array $data
   * @throws Exception
   */
  private function sanitizePersonen(string $name, array &$data) {
    switch ($name) {
      case "mollie_customer_id":
        $data[$name] = $data[0];
        break;
      case "achternaam":
        $naamDelen = explode(" ", $data[1]);
        if (count($naamDelen) > 1) {
          $data['voornaam'] = trim($naamDelen[0]);
          unset($naamDelen[0]);
          $data['achternaam'] = trim(implode(" ", $naamDelen));
        }
        else {
          if (!empty($naamDelen[0])) {
            $data['achternaam'] = trim($naamDelen[0]);
          }
          else {
            $data['achternaam'] = "Onbekend " . rand(0,9999);
          }
          $data['voornaam'] = "Onbekend " . rand(0,9999);
        }
        break;
      case "email":
        $data[$name] = $data[2];
        break;
    }
  }

  /**
   * Method to close the file
   */
  public function close() {
    fclose($this->csv);
  }

  /**
   * Method to set the headers of the csv file
   *
   * @param $headers
   */
  private function setHeaders($headers) {
    $this->_headers = $headers;
  }

}
