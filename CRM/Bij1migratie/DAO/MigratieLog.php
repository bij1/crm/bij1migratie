<?php

/**
 * @package CRM
 * @copyright CiviCRM LLC https://civicrm.org/licensing
 *
 * Generated from bij1migratie/xml/schema/CRM/Bij1migratie/MigratieLog.xml
 * DO NOT EDIT.  Generated by CRM_Core_CodeGen
 * (GenCodeChecksum:186c4c3ad500e8ffcedca1378455c6d9)
 */
use CRM_Bij1migratie_ExtensionUtil as E;

/**
 * Database access object for the MigratieLog entity.
 */
class CRM_Bij1migratie_DAO_MigratieLog extends CRM_Core_DAO {
  const EXT = E::LONG_NAME;
  const TABLE_ADDED = '';

  /**
   * Static instance to hold the table name.
   *
   * @var string
   */
  public static $_tableName = 'civicrm_migratie_log';

  /**
   * Should CiviCRM log any modifications to this table in the civicrm_log table.
   *
   * @var bool
   */
  public static $_log = TRUE;

  /**
   * Unique MigratieLog ID
   *
   * @var int
   */
  public $id;

  /**
   * customer ID uit Mollie
   *
   * @var string
   */
  public $mollie_customer_id;

  /**
   * Datum migratie
   *
   * @var datetime
   */
  public $migratiedatum;

  /**
   * Type migratie
   *
   * @var string
   */
  public $type_migratie;

  /**
   * Type melding
   *
   * @var string
   */
  public $type_melding;

  /**
   * Melding
   *
   * @var text
   */
  public $melding;

  /**
   * Class constructor.
   */
  public function __construct() {
    $this->__table = 'civicrm_migratie_log';
    parent::__construct();
  }

  /**
   * Returns localized title of this entity.
   *
   * @param bool $plural
   *   Whether to return the plural version of the title.
   */
  public static function getEntityTitle($plural = FALSE) {
    return $plural ? E::ts('Migratie Logs') : E::ts('Migratie Log');
  }

  /**
   * Returns all the column names of this table
   *
   * @return array
   */
  public static function &fields() {
    if (!isset(Civi::$statics[__CLASS__]['fields'])) {
      Civi::$statics[__CLASS__]['fields'] = [
        'id' => [
          'name' => 'id',
          'type' => CRM_Utils_Type::T_INT,
          'description' => E::ts('Unique MigratieLog ID'),
          'required' => TRUE,
          'where' => 'civicrm_migratie_log.id',
          'table_name' => 'civicrm_migratie_log',
          'entity' => 'MigratieLog',
          'bao' => 'CRM_Bij1migratie_DAO_MigratieLog',
          'localizable' => 0,
          'html' => [
            'type' => 'Number',
          ],
          'readonly' => TRUE,
          'add' => NULL,
        ],
        'mollie_customer_id' => [
          'name' => 'mollie_customer_id',
          'type' => CRM_Utils_Type::T_STRING,
          'title' => E::ts('Mollie Customer ID'),
          'description' => E::ts('customer ID uit Mollie'),
          'maxlength' => 32,
          'size' => CRM_Utils_Type::MEDIUM,
          'where' => 'civicrm_migratie_log.mollie_customer_id',
          'table_name' => 'civicrm_migratie_log',
          'entity' => 'MigratieLog',
          'bao' => 'CRM_Bij1migratie_DAO_MigratieLog',
          'localizable' => 0,
          'html' => [
            'type' => 'Text',
          ],
          'add' => NULL,
        ],
        'migratiedatum' => [
          'name' => 'migratiedatum',
          'type' => CRM_Utils_Type::T_DATE + CRM_Utils_Type::T_TIME,
          'title' => E::ts('Datum migratie'),
          'description' => E::ts('Datum migratie'),
          'where' => 'civicrm_migratie_log.migratiedatum',
          'table_name' => 'civicrm_migratie_log',
          'entity' => 'MigratieLog',
          'bao' => 'CRM_Bij1migratie_DAO_MigratieLog',
          'localizable' => 0,
          'html' => [
            'type' => 'Select Date',
            'formatType' => 'activityDate',
          ],
          'add' => NULL,
        ],
        'type_migratie' => [
          'name' => 'type_migratie',
          'type' => CRM_Utils_Type::T_STRING,
          'title' => E::ts('Type migratie'),
          'description' => E::ts('Type migratie'),
          'maxlength' => 32,
          'size' => CRM_Utils_Type::MEDIUM,
          'where' => 'civicrm_migratie_log.type_migratie',
          'table_name' => 'civicrm_migratie_log',
          'entity' => 'MigratieLog',
          'bao' => 'CRM_Bij1migratie_DAO_MigratieLog',
          'localizable' => 0,
          'html' => [
            'type' => 'Text',
          ],
          'add' => NULL,
        ],
        'type_melding' => [
          'name' => 'type_melding',
          'type' => CRM_Utils_Type::T_STRING,
          'title' => E::ts('Type melding'),
          'description' => E::ts('Type melding'),
          'maxlength' => 32,
          'size' => CRM_Utils_Type::MEDIUM,
          'where' => 'civicrm_migratie_log.type_melding',
          'table_name' => 'civicrm_migratie_log',
          'entity' => 'MigratieLog',
          'bao' => 'CRM_Bij1migratie_DAO_MigratieLog',
          'localizable' => 0,
          'html' => [
            'type' => 'Text',
          ],
          'add' => NULL,
        ],
        'melding' => [
          'name' => 'melding',
          'type' => CRM_Utils_Type::T_TEXT,
          'title' => E::ts('Melding'),
          'description' => E::ts('Melding'),
          'rows' => 8,
          'cols' => 180,
          'where' => 'civicrm_migratie_log.melding',
          'table_name' => 'civicrm_migratie_log',
          'entity' => 'MigratieLog',
          'bao' => 'CRM_Bij1migratie_DAO_MigratieLog',
          'localizable' => 0,
          'html' => [
            'type' => 'TextArea',
          ],
          'add' => NULL,
        ],
      ];
      CRM_Core_DAO_AllCoreTables::invoke(__CLASS__, 'fields_callback', Civi::$statics[__CLASS__]['fields']);
    }
    return Civi::$statics[__CLASS__]['fields'];
  }

  /**
   * Return a mapping from field-name to the corresponding key (as used in fields()).
   *
   * @return array
   *   Array(string $name => string $uniqueName).
   */
  public static function &fieldKeys() {
    if (!isset(Civi::$statics[__CLASS__]['fieldKeys'])) {
      Civi::$statics[__CLASS__]['fieldKeys'] = array_flip(CRM_Utils_Array::collect('name', self::fields()));
    }
    return Civi::$statics[__CLASS__]['fieldKeys'];
  }

  /**
   * Returns the names of this table
   *
   * @return string
   */
  public static function getTableName() {
    return self::$_tableName;
  }

  /**
   * Returns if this table needs to be logged
   *
   * @return bool
   */
  public function getLog() {
    return self::$_log;
  }

  /**
   * Returns the list of fields that can be imported
   *
   * @param bool $prefix
   *
   * @return array
   */
  public static function &import($prefix = FALSE) {
    $r = CRM_Core_DAO_AllCoreTables::getImports(__CLASS__, 'migratie_log', $prefix, []);
    return $r;
  }

  /**
   * Returns the list of fields that can be exported
   *
   * @param bool $prefix
   *
   * @return array
   */
  public static function &export($prefix = FALSE) {
    $r = CRM_Core_DAO_AllCoreTables::getExports(__CLASS__, 'migratie_log', $prefix, []);
    return $r;
  }

  /**
   * Returns the list of indices
   *
   * @param bool $localize
   *
   * @return array
   */
  public static function indices($localize = TRUE) {
    $indices = [];
    return ($localize && !empty($indices)) ? CRM_Core_DAO_AllCoreTables::multilingualize(__CLASS__, $indices) : $indices;
  }

}
