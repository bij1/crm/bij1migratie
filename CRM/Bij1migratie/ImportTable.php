<?php

use CRM_Bij1migratie_ExtensionUtil as E;
/**
 * Class voor de tabel uit de csv import (BIJ1 migratie)
 *
 * @author Erik Hommel (CiviCooP) <erik.hommel@civicoop.org>
 * @date 9 dec 2021
 * @license AGPL-3.0
 */
class CRM_Bij1migratie_ImportTable {

  private $_tableName;

  /**
   * Method to generate the name of the table
   *
   * @param string $fileName
   */
  public function generateTableName(string $fileName) {
    $tableName = strtolower(substr($fileName,0,48)) . "_" . date('YmdHis');
    // check if table already exists and if so, and random number to name
    if (CRM_Core_DAO::checkTableExists($tableName)) {
      $tableName .= "_" . rand(0,99);
    }
    $this->_tableName = $tableName;
  }

  /**
   * Method to set the table name
   *
   * @param $tableName
   */
  public function setTableName($tableName) {
    $this->_tableName = $tableName;
  }

  /**
   * Method to get the table name
   *
   * @return mixed
   */
  public function getTableName() {
    return $this->_tableName;
  }

  /**
   * Method to create the table
   *
   * @param string $migratieType
   */
  public function createTable(string $migratieType) {
    $headers = Civi::service('bij1Migratie')->getMigratieColumns($migratieType);
    // create a column for each header
    $columns = ['id int UNSIGNED NOT NULL AUTO_INCREMENT'];
    foreach ($headers as $civiName => $sourceName) {
      $columns[] = $civiName . " VARCHAR(256)";
    }
    // if table exists, drop old one
    if (CRM_Core_DAO::checkTableExists($this->_tableName)) {
      $this->destroy();
    }
    $create = "CREATE TABLE " . $this->_tableName . " (" . implode(", ", $columns) .", PRIMARY KEY (id) )";
    CRM_Core_DAO::executeQuery($create);
  }

  /**
   * Function to write row in table using data from incoming array
   *
   * @param $data
   * @return bool
   */
  public function writeRow($data) {
    if (empty($data)) {
      return FALSE;
    }
    if (!is_array($data)) {
      $data = [$data];
    }
    $i = 0;
    $elements = [];
    $columns = [];
    $queryParams = [];
    foreach ($data as $key => $value) {
      $value = CRM_Core_DAO::escapeString($value);
      $i++;
      $elements[] = "%" . $i;
      $columns[] = $key;
      $queryParams[$i] = [$value, "String"];
    }
    $query = "INSERT INTO ". $this->_tableName . " (" . implode(",", $columns) . ") VALUES (" . implode(",", $elements) . ")";
    try {
      CRM_Core_DAO::executeQuery($query,$queryParams);
    }
    catch (Exception $ex) {
      CRM_Core_Session::setStatus("Kon geen migratie regel in tabel schrijven, check CiviCRM error log", "Kon migratierecord niet schrijven", "error");
      Civi::log()->error("on geen migratie regel in tabel schrijven in " . __METHOD__ . ", foutboodschap: " . $ex->getMessage());
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Method to get all rows
   *
   * @param string $migratieType
   * @param string $tableName
   * @return array|false
   */
  public function getAllRows(string $migratieType, string $tableName) {
    if ($tableName) {
      $result = [];
      $query = "SELECT * FROM " . $tableName;
      $dao = CRM_Core_DAO::executeQuery($query);
      while ($dao->fetch()) {
        $result[] = Civi::service('bij1Algemeen')->moveDaoToArray($dao);
      }
      return $result;
    }
    return FALSE;
  }

  /**
   * Method to get the column names
   *
   * @param string $tableName
   * @return array
   */
  public function getColumnNames(string $tableName) {
    $result = [];
    if (empty($tableName)) {
      $tableName = $this->_tableName;
    }
    if ($tableName) {
      $dao = CRM_Core_DAO::executeQuery("SHOW COLUMNS FROM " . $tableName);
      while ($dao->fetch()) {
        if ($dao->Field != "id") {
          $result[] = $dao->Field;
        }
      }
    }
    return $result;
  }

  /**
   * Method to drop table
   */
  public function destroy() {
    if ($this->_tableName) {
      CRM_Core_DAO::executeQuery("DROP TABLE IF EXISTS " . $this->_tableName);
    }
  }


}

