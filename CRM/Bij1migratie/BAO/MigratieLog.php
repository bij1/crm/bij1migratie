<?php
use CRM_Bij1migratie_ExtensionUtil as E;

class CRM_Bij1migratie_BAO_MigratieLog extends CRM_Bij1migratie_DAO_MigratieLog {

  /**
   * Create a new MigratieLog based on array-data
   *
   * @param array $params key-value pairs
   * @return CRM_Bij1migratie_DAO_MigratieLog|NULL
   *
  public static function create($params) {
    $className = 'CRM_Bij1migratie_DAO_MigratieLog';
    $entityName = 'MigratieLog';
    $hook = empty($params['id']) ? 'create' : 'edit';

    CRM_Utils_Hook::pre($hook, $entityName, CRM_Utils_Array::value('id', $params), $params);
    $instance = new $className();
    $instance->copyValues($params);
    $instance->save();
    CRM_Utils_Hook::post($hook, $entityName, $instance->id, $instance);

    return $instance;
  } */

}
