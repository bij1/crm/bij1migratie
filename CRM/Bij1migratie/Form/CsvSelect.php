<?php

use CRM_Bij1migratie_ExtensionUtil as E;

/**
 * Form controller class
 *
 * @see https://docs.civicrm.org/dev/en/latest/framework/quickform/
 */
class CRM_Bij1migratie_Form_CsvSelect extends CRM_Core_Form {

  private $_tableName = NULL;

  /**
   * Overridden parent method to build the form
   */
  public function buildQuickForm() {
    $this->add('file', 'csv_file', E::ts('Migratie bestand (CSV)'), [], TRUE);
    $this->addRule('csv_file', E::ts('Bestand moet CSV formaat hebben'), 'utf8File');
    $this->addRule('csv_file', E::ts('Er moet een geldig bestand geladen worden.'), 'uploadedfile');
    $this->add('advcheckbox', 'first_row_headers', E::ts('Bevat de eerste rij kolomkoppen?'));
    $separatorList = Civi::service('bij1Migratie')->getSeparatorList();
    $this->add('select', 'migratie_type', E::ts('Soort migratie'), ['A' => 'Airtable', 'P' => 'Mollie klanten', 'B' => 'Betalingen'] , TRUE);
    $this->add('select', 'separator_id', E::ts('Scheidingsteken voor velden'), $separatorList , TRUE);
    $this->addButtons([
      ['type' => 'next', 'name' => E::ts('Next'), 'isDefault' => TRUE],
      ['type' => 'cancel', 'name' => E::ts('Cancel')],
    ]);
    parent::buildQuickForm();
  }

  /**
   * Overridden parent method to set default values
   *
   * @return array
   */
  public function setDefaultValues()   {
    $defaults = [];
    $defaults['first_row_headers'] = TRUE;
    $defaults['separator_id'] = 1;
    return $defaults;
  }

  /**
   * Overridden parent method to add validation rules
   *
   */
  public function addRules() {
    $this->addFormRule(['CRM_Bij1migratie_Form_CsvSelect', 'validateCsvFile']);
  }

  /**
   * Overridden parent method to prepare the form
   */
  public function preProcess() {
    CRM_Utils_System::setTitle("BIJ1 - CiviCRM migratie - kies CSV bestand");
    parent::preProcess(); // TODO: Change the autogenerated stub
  }

  /**
   * Overridden parent method to process the form
   */
  public function postProcess() {
    set_time_limit(0);
    // send csv data to upload table
    $table = $this->populateTable(Civi::service('bij1Migratie')->getSeparator($this->_submitValues['separator_id']));
    if ($table) {
      CRM_Bij1migratie_Migratie::migreer($this->_submitValues['migratie_type'], $this->_tableName);
    }
    parent::postProcess();
  }

  /**
   * Method to get only bit before . for filename
   *
   * @param $fileName
   * @return string
   */
  private function sanitizeFileName($fileName) {
    $fileName = trim(strtolower($fileName));
    $parts = explode(".", $fileName);
    $fileName = $parts[0];
    $invalids = [" ", "/", "\\", "#", "~", "`", "@", "%", "^", "&", "*", "(", ")", "{", "}", "[", "]", "<", ">", "?", ",", ":", ";"];
    foreach ($invalids as $invalid) {
      $fileName = str_replace($invalid, "_", $fileName);
    }
    if (strlen($fileName) > 53) {
      $fileName = substr($fileName, 0, 53);
    }
    return $fileName;
  }

  /**
   * Method to populate table with data from selected csv file
   *
   * @param $separator
   * @return bool
   * @throws Exception
   */
  private function populateTable($separator) {
    $table = new CRM_Bij1migratie_ImportTable();
    $table->generateTableName($this->sanitizeFileName($this->_submitFiles['csv_file']['name']));
    $this->_tableName = $table->getTableName();
    $csv = new CRM_Bij1migratie_CsvFile();
    $csv->initialize($this->_submitFiles['csv_file']['tmp_name'], $this->_submitValues['migratie_type'], $this->_submitValues['first_row_headers'], $separator);
    $table->createTable($this->_submitValues['migratie_type']);
    $csv->open();
    while (!feof($csv->csv)) {
      $data = $csv->readNext($this->_tableName);
      if (!empty($data)) {
        $table->writeRow($data);
      }
    }
    $csv->close();
    return TRUE;
  }
  /**
   * Method to validate if file has the correct ext and can be opened
   *
   * @param $fields
   * @param $files
   * @return bool|array
   * @throws
   */
  public static function validateCsvFile($fields, $files) {
    $ext = pathinfo($files['csv_file']['name'], PATHINFO_EXTENSION);
    if ($ext != "csv" && $ext != "txt") {
      $errors['csv_file'] = "Alleen CSV bestanden kunnen gemigreerd worden.";
      return $errors;
    }
    if (!isset($files['csv_file']['tmp_name'])) {
      $errors['csv_file'] = "Er lijkt geen bestand geladen te zijn.";
      return $errors;
    }
    $check = fopen($files['csv_file']['tmp_name'], 'r');
    if (!$check) {
      $errors['csv_file'] = "Kan het CSV bestand niet openen.";
      return $errors;
    }
    fclose($check);
    return TRUE;
  }

}
