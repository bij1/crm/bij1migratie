<?php
use CRM_Bij1migratie_ExtensionUtil as E;

/**
 * Class voor BIJ1 migratie contact verwerking
 *
 * @author Erik Hommel (CiviCooP) <erik.hommel@civicoop.org>
 * @date 21 dec 2021
 * @license AGPL-3.0
 */
class CRM_Bij1migratie_Contact {

  /**
   * Method om contact ID op te halen met mollie customer id
   *
   * @param string $mollieCustomerId
   * @return false|string
   */
  public function haalContactIdMetMollieCustomerId(string $mollieCustomerId) {
    if (!empty($mollieCustomerId)) {
      $identifierType = Civi::service('bij1Algemeen')->getMollieCustomerIdIdentityType();
      if ($identifierType) {
        $query = "SELECT entity_id FROM civicrm_value_contact_id_history WHERE identifier_type = %1 AND identifier = %2";
        $contactId = CRM_Core_DAO::singleValueQuery($query, [
          1 => [$identifierType, "String"],
          2 => [$mollieCustomerId, "String"],
        ]);
        if ($contactId) {
          return (int) $contactId;
        }
      }
    }
    return FALSE;
  }

  /**
   * Method om contact met email op te zoeken
   *
   * @param $email
   * @return false|int
   * @throws API_Exception
   */
  public function haalContactIdMetEmail($email) {
    if (!empty($email)) {
      $contactId = NULL;
      $count = CRM_Core_DAO::singleValueQuery("SELECT COUNT(DISTINCT(contact_id)) FROM civicrm_email WHERE email = %1", [1 => [$email, "String"]]);
      if ($count > 1) {
        $datum = new DateTime();
        \Civi\Api4\MigratieLog::create()
          ->addValue('mollie_customer_id', "n.v.t.")
          ->addValue('migratiedatum', $datum->format("y-m-d"))
          ->addValue('type_migratie', "B")
          ->addValue('type_melding', 'waarschuwing')
          ->addValue('melding', 'Waarschuwing, meerdere contacten met email: ' . $email . ', betaling aan eerste toegekend. Nakijken!')
          ->execute();
        $query = "SELECT contact_id FROM civicrm_email WHERE email = %1 LIMIT 1";
        $contactId = CRM_Core_DAO::singleValueQuery($query, [1 => [$email, "String"]]);
      }
      if ($count == 1) {
        $query = "SELECT contact_id FROM civicrm_email WHERE email = %1";
        $contactId = CRM_Core_DAO::singleValueQuery($query, [1 => [$email, "String"]]);
      }
      if ($contactId) {
        return (int) $contactId;
      }
    }
    return FALSE;
  }

  /**
   * Method om klant te halen of te maken via de contacten migratie form processor
   *
   * @param array $data
   * @param DateTime $migratieDatum
   * @return false|int
   * @throws API_Exception
   * @throws \Civi\API\Exception\UnauthorizedException
   */
  public function haalOfMaak(array $data, DateTime $migratieDatum) {
    try {
      $result = civicrm_api3('FormProcessor', 'bij1_contacten_migratie', $data);
      if (isset($result['action'][2]['output']['contact_id'])) {
        return (int) $result['action'][2]['output']['contact_id'];
      }
    }
    catch (CiviCRM_API3_Exception $ex) {
      \Civi\Api4\MigratieLog::create()
        ->addValue('mollie_customer_id', $data['mollie_customer_id'])
        ->addValue('migratiedatum', $migratieDatum->format("y-m-d"))
        ->addValue('type_migratie', "A")
        ->addValue('type_melding', 'fout')
        ->addValue('melding', 'FOUT, melding van FormProcessor in CRM_Bij1migratie_Contact method haalOfMaak : ' . $ex->getMessage())
        ->execute();
    }
    return FALSE;
  }

}
