<?php
use CRM_Bij1migratie_ExtensionUtil as E;

/**
 * Verwerking van migratie
 */
class CRM_Bij1migratie_Migratie {
  /**
   * Method om data te migreren
   *
   * @param string $migratieType
   * @param string $tableName
   */
  public static function migreer(string $migratieType, string $tableName) {
    set_time_limit(0);
    $table = new CRM_Bij1migratie_ImportTable();
    $data = $table->getAllRows($migratieType, $tableName);
    $migratieDatum = new DateTime('now');
    if ($data) {
      foreach ($data as $rij) {
        if (!empty($rij)) {
          switch ($migratieType) {
            case "A":
              self::migreerAirtable($migratieType, $migratieDatum, $rij);
              break;
            case "B":
              self::migreerBetaling($migratieType, $migratieDatum, $rij);
              break;
            case "P":
              self::migreerContact($migratieType, $migratieDatum, $rij);
              break;
          }
        }
      }
    }
  }

  /**
   * Method om betalingen te migreren
   *
   * @param string $migratieType
   * @param DateTime $migratieDatum
   * @param array $rij
   * @throws API_Exception
   * @throws \Civi\API\Exception\UnauthorizedException
   */
  private static function migreerBetaling(string $migratieType, DateTime $migratieDatum, array $rij) {
    $mollieCustomerId = NULL;
    $email = NULL;
    // alleen als paid of pending
    if ($rij['status'] == "paid" || $rij['status'] == "pending") {
      if (isset($rij['mollie_customer_id']) && !empty($rij['mollie_customer_id'])) {
        $mollieCustomerId = trim($rij['mollie_customer_id']);
      }
      if (isset($rij['email']) && !empty($rij['email'])) {
        $email = $rij['email'];
      }
      if ($mollieCustomerId || $email) {
        $contact = new CRM_Bij1migratie_Contact();
        $contactId = $contact->haalContactIdMetMollieCustomerId($rij['mollie_customer_id']);
        if (!$contactId && isset($rij['email']) && !empty($rij['email'])) {
          $contactId = $contact->haalContactIdMetEmail($rij['email']);
        }
        if ($contactId) {
          if ($rij['status'] == "paid") {
            $rij['status'] = Civi::service('bij1Migratie')->getCompletedContributionStatusId();
          } else {
            $rij['status'] = Civi::service('bij1Migratie')->getPendingContributionStatusId();
          }
          if (!self::bestaatBetaling($contactId, $rij)) {
            try {
              $bijdrage = \Civi\Api4\Contribution::create()
                ->addValue('contact_id', $contactId)
                ->addValue('financial_type_id', Civi::service('bij1Migratie')->getFinancialTypeId())
                ->addValue('payment_instrument_id', $rij['betalingsinstrument'])
                ->addValue('receive_date', $rij['aanmaakdatum'])
                ->addValue('total_amount', $rij['bedrag'])
                ->addValue('contribution_status_id', $rij['status'])
                ->addValue('trxn_id', $rij['transactie_id'])
                ->addValue('mollie_betaling_data.mandaat_id', $rij['mandaat'])
                ->addValue('mollie_betaling_data.klantnaam', $rij['klant'])
                ->addValue('mollie_betaling_data.klantemail', $rij['email']);
              if (isset($rij['omschrijving']) && !empty($rij['omschrijving'])) {
                $bijdrage->addValue('source', $rij['omschrijving']);
              }
              if (isset($rij['betalingsdatum'])) {
                $bijdrage->addValue('receipt_date', $rij['betalingsdatum']);
              }
              $bijdrage->execute();
            } catch (API_Exception $ex) {
              \Civi\Api4\MigratieLog::create()
                ->addValue('mollie_customer_id', $rij['mollie_customer_id'])
                ->addValue('migratiedatum', $migratieDatum->format("y-m-d"))
                ->addValue('type_migratie', $migratieType)
                ->addValue('type_melding', 'fout')
                ->addValue('melding', 'FOUT, melding van API4 Contribution Create: ' . $ex->getMessage())
                ->execute();
            }
          }
          else {
            \Civi\Api4\MigratieLog::create()
              ->addValue('mollie_customer_id', $rij['mollie_customer_id'])
              ->addValue('migratiedatum', $migratieDatum->format("y-m-d"))
              ->addValue('type_migratie', $migratieType)
              ->addValue('type_melding', 'waarschuwing')
              ->addValue('melding', 'Waarschuwing, betaling bestaat al dus niet gemigreerd, data: ' . json_encode($rij))
              ->execute();
          }
        }
        else {
          \Civi\Api4\MigratieLog::create()
            ->addValue('mollie_customer_id', $rij['mollie_customer_id'])
            ->addValue('migratiedatum', $migratieDatum->format("y-m-d"))
            ->addValue('type_migratie', $migratieType)
            ->addValue('type_melding', 'fout')
            ->addValue('melding', 'FOUT, geen contact gevonden met mollie customer id: ' . $rij['mollie_customer_id'] . ' of met email: ' . $rij['email'])
            ->execute();
        }
      }
      else {
        \Civi\Api4\MigratieLog::create()
          ->addValue('mollie_customer_id', $rij['mollie_customer_id'])
          ->addValue('migratiedatum', $migratieDatum->format("y-m-d"))
          ->addValue('type_migratie', $migratieType)
          ->addValue('type_melding', 'fout')
          ->addValue('melding', 'FOUT, geen mollie customer id of email gevonden in data: ' . json_encode($rij))
          ->execute();
      }
    }
  }

  /**
   * Method om contact te migreren
   *
   * @param string $migratieType
   * @param DateTime $migratieDatum
   * @param $rij
   * @throws API_Exception
   */
  private static function migreerContact(string $migratieType, DateTime $migratieDatum, $rij) {
    $rij['locatietype'] = Civi::service('bij1Algemeen')->getDefaultLocationTypeId();
    try {
      civicrm_api3('FormProcessor', 'bij1_contacten_migratie', $rij);
      \Civi\Api4\MigratieLog::create()
        ->addValue('mollie_customer_id', $rij['mollie_customer_id'])
        ->addValue('migratiedatum', $migratieDatum->format("y-m-d"))
        ->addValue('type_migratie', $migratieType)
        ->addValue('type_melding', 'succes')
        ->addValue('melding', 'Gemigreerd')
        ->execute();
    }
    catch (CiviCRM_API3_Exception $ex) {
      \Civi\Api4\MigratieLog::create()
        ->addValue('mollie_customer_id', $rij['mollie_customer_id'])
        ->addValue('migratiedatum', $migratieDatum->format("y-m-d"))
        ->addValue('type_migratie', $migratieType)
        ->addValue('type_melding', 'fout')
        ->addValue('melding', 'FOUT, melding van FormProcessor bij1_contacten_migratie API call: ' . $ex->getMessage())
        ->execute();
    }
  }

  /**
   * Method voor migratie airtable
   *
   * @param string $migratieType
   * @param DateTime $migratieDatum
   * @param array $rij
   * @throws API_Exception
   */
  private static function migreerAirtable(string $migratieType, DateTime $migratieDatum, array $rij) {
    $contact = new CRM_Bij1migratie_Contact();
    $rij['locatietype'] = Civi::service('bij1Algemeen')->getDefaultLocationTypeId();
    // eerst beoordelen of het contact gevonden kan worden met Mollie Customer Id
    $contactId = $contact->haalContactIdMetMollieCustomerId($rij['mollie_customer_id']);
    // als niks gevonden gebruik Form Processor om contact te vinden of te maken
    if (!$contactId) {
      $contactId = $contact->haalOfMaak($rij, $migratieDatum);
    }
    if ($contactId) {
      $rij['contact_id'] = $contactId;
      // verwerk de rest (email, adres, telefoon, notities, groepen, lidnummer, inschrijfdatum via Form Processor
      try {
        civicrm_api3('FormProcessor', 'bij1_airtable_migratie', $rij);
        // voeg bankrekening toe indien van toepassing
        if (isset($rij['iban']) && !empty($rij['iban'])) {
          self::maakBankrekening($migratieDatum, $contactId, $rij['iban']);
        }
      }
      catch (CiviCRM_API3_Exception $ex) {
          \Civi\Api4\MigratieLog::create()
            ->addValue('mollie_customer_id', $rij['mollie_customer_id'])
            ->addValue('migratiedatum', $migratieDatum->format("y-m-d"))
            ->addValue('type_migratie', $migratieType)
            ->addValue('type_melding', 'fout')
            ->addValue('melding', 'FOUT, melding van FormProcessor bij1_airtable_migratie API call: ' . $ex->getMessage())
            ->execute();
        }

    }
  }

  /**
   * Method om te beoordelen of gift al bestaat voor contact
   *
   * @param int $contactId
   * @param array $betaling
   * @return bool
   * @throws Exception
   */
  public static function bestaatBetaling(int $contactId, array $betaling) {
    $verplichten = ["bedrag", "aanmaakdatum"];
    foreach ($verplichten as $verplicht) {
      if (!isset($betaling[$verplicht]) || empty($betaling[$verplicht])) {
        return FALSE;
      }
      $receiveDate = new DateTime($betaling['aanmaakdatum']);
      $query = "SELECT COUNT(*) FROM civicrm_contribution WHERE contact_id = %1 AND receive_date = %2 AND total_amount = %3";
      $count = CRM_Core_DAO::singleValueQuery($query, [
        1 => [$contactId, "Integer"],
        2 => [$receiveDate->format("Y-m-d"), "String"],
        3 => [$betaling['bedrag'], "String"],
      ]);
      if ($count > 0) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Method om te bepalen of de persoon al bestaat
   *
   * @param int $ssaskiaNummer
   * @return bool
   */
  public static function bestaatPersoon(int $ssaskiaNummer) {
    $query = "SELECT COUNT(*) FROM civicrm_value_pax_contact_data WHERE ssaskia_nummer = %1";
    $count = CRM_Core_DAO::singleValueQuery($query, [1 => [$ssaskiaNummer, "Integer"]]);
    if ($count > 0) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Method om migratie groep aan te maken
   */
  public function maakMigratieGroepen() {
    $query = "SELECT COUNT(*) FROM civicrm_group WHERE name = %1";
    $count = CRM_Core_DAO::singleValueQuery($query, [1 => ["pax_migratie_groep", "String"]]);
    if ($count == 0) {
      try {
        \Civi\Api4\Group::create()
          ->addValue('name', 'pax_migratie_groep')
          ->addValue('title', 'CiviCRM Migratie')
          ->addValue('description', 'Groep met personen en organisaties die tijdens de migratie aangemaakt werden')
          ->addValue('is_active', TRUE)
          ->addValue('is_reserved', TRUE)
          ->execute();
      }
      catch (API_Exception $ex) {
        Civi::log()->error(E::ts("Kon geen groep voor migratie aanmaken, fout van API4 Group create: ") . $ex->getMessage());
      }
    }
    $count = CRM_Core_DAO::singleValueQuery($query, [1 => ["pax_dm_mag", "String"]]);
    if ($count == 0) {
      try {
        \Civi\Api4\Group::create()
          ->addValue('name', 'pax_dm_mag')
          ->addValue('title', 'DM Mag')
          ->addValue('description', 'Groep van personen die in sSaskia de vlag DM mag aan hadden staan')
          ->addValue('is_active', TRUE)
          ->addValue('is_reserved', TRUE)
          ->execute();
      }
      catch (API_Exception $ex) {
        Civi::log()->error(E::ts("Kon geen groep voor dm mag aanmaken, fout van API4 Group create: ") . $ex->getMessage());
      }
    }
    $count = CRM_Core_DAO::singleValueQuery($query, [1 => ["pax_post_mag", "String"]]);
    if ($count == 0) {
      try {
        \Civi\Api4\Group::create()
          ->addValue('name', 'pax_post_mag')
          ->addValue('title', 'Post Mag')
          ->addValue('description', 'Groep van personen die in sSaskia de vlag post mag aan hadden staan')
          ->addValue('is_active', TRUE)
          ->addValue('is_reserved', TRUE)
          ->execute();
      }
      catch (API_Exception $ex) {
        Civi::log()->error(E::ts("Kon geen groep voor post mag aanmaken, fout van API4 Group create: ") . $ex->getMessage());
      }
    }
  }

  /**
   * Method om bankrekening toe te voegen
   *
   * @param DateTime $migratieDatum
   * @param int $contactId
   * @param string $iban
   * @throws API_Exception
   */
  public static function maakBankrekening(DateTime $migratieDatum, int $contactId, string $iban) {
    if (!empty($iban) && !empty($contactId)) {
      $iban = trim($iban);
      // alleen als rekeningnummer nog niet bestaat voor de donor
      if (!self::bestaatBankrekening($contactId, $iban)) {
        try {
          $rekening = civicrm_api3('BankingAccount', 'create', [
            'contact_id' => $contactId,
            'description' => 'rekening vanuit migratie airtable',
            'created_date' => date('YmdHis'),
            'data_raw' => '{}',
          ]);
          // add a reference
          civicrm_api3('BankingAccountReference', 'create', [
            'reference' => $iban,
            'reference_type_id' => Civi::service('bij1Algemeen')->getIbanAccountReference(),
            'ba_id' => $rekening['id'],
          ]);
        } catch (CiviCRM_API3_Exception $ex) {
          \Civi\Api4\MigratieLog::create()
            ->addValue('mollie_customer_id', $contactId)
            ->addValue('migratiedatum', $migratieDatum->format("y-m-d"))
            ->addValue('type_migratie', 'A')
            ->addValue('type_melding', 'fout')
            ->addValue('melding', 'FOUT, geen bankrekening aan kunnen maken voor contact ID ' . $contactId . ' met IBAN ' . $iban)
            ->execute();
        }
      }
    }
  }

  /**
   * Method om te bekijken of het rekening nummer al bestaat voor het contact
   *
   * @param int $contactId
   * @param string $rekening
   * @return bool
   */
  public static function bestaatBankrekening(int $contactId, string $rekening) {
    $query = "SELECT COUNT(*)
        FROM civicrm_bank_account cba
            JOIN civicrm_bank_account_reference cbar ON cba.id = cbar.ba_id
        WHERE cba.contact_id = %1 AND cbar.reference = %2";
    $count = CRM_Core_DAO::singleValueQuery($query, [
      1 => [$contactId, "Integer"],
      2 => [$rekening, "String"],
    ]);
    if ($count > 0) {
      return TRUE;
    }
    return FALSE;
  }
}
