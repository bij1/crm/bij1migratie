<div class="crm-block crm-form-block">
  <div class="help-block" id="help">
      {ts}Selecteer het import bestand (type CSV), of de eerste rij de kolomkoppen bevat en welk teken gebruikt wordt als veldscheiding.{/ts}<br />
  </div>
  <div class="crm-submit-buttons">
      {include file="CRM/common/formButtons.tpl" location="top"}
  </div>

  <div class="crm-section csv_file_section">
    <div class="label">{$form.migratie_type.label}</div>
    <div class="content">{$form.migratie_type.html}</div>
    <div class="clear"></div>
  </div>

  <div class="crm-section csv_file_section">
    <div class="label">{$form.csv_file.label}</div>
    <div class="content">{$form.csv_file.html}</div>
    <div class="clear"></div>
  </div>

  <div class="crm-section first_row_headers_section">
    <div class="label">{$form.first_row_headers.label}</div>
    <div class="content">{$form.first_row_headers.html}</div>
    <div class="clear"></div>
  </div>

  <div class="crm-section separator_id_section">
    <div class="label">{$form.separator_id.label}</div>
    <div class="content">{$form.separator_id.html}</div>
    <div class="clear"></div>
  </div>

    {* FOOTER *}
  <div class="crm-submit-buttons">
      {include file="CRM/common/formButtons.tpl" location="bottom"}
  </div>
</div>
