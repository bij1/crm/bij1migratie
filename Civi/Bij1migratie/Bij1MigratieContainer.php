<?php
/**
 * @author Erik Hommel <erik.hommel@civicoop.org>
 * @date 13 Oct 2021
 * @license AGPL-3.0
 */
namespace Civi\Bij1migratie;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use CRM_Bij1migratie_ExtensionUtil as E;

class Bij1MigratieContainer implements CompilerPassInterface {

  /**
   * You can modify the container here before it is dumped to PHP code.
   */
  public function process(ContainerBuilder $container) {
    $definition = new Definition('CRM_Bij1migratie_Bij1MigratieService');
    $definition->setFactory(['CRM_Bij1migratie_Bij1MigratieService', 'getInstance']);
    $this->setAanhef($definition);
    $this->setGeslacht($definition);
    $this->setFinancieelType($definition);
    $this->setContributionStatus($definition);
    $this->setPaymentInstrumentIds($definition);
    $definition->setPublic(TRUE);
    $container->setDefinition('bij1Migratie', $definition);
  }

  /**
   * Method om betalingsmethodes vast te stellen
   *
   * @param $definition
   */
  private function setPaymentInstrumentIds(&$definition) {
    $query = "SELECT cov.value, cov.name FROM civicrm_option_group cog JOIN civicrm_option_value cov ON cog.id = cov.option_group_id
        WHERE cog.name = %1 AND cov.name IN(%2, %3, %4, %5)";
    $dao = \CRM_Core_DAO::executeQuery($query, [
      1 => ["payment_instrument", "String"],
      2 => ["EFT", "String"],
      3 => ["mollie_frst", "String"],
      4 => ["mollie_rcur", "String"],
      5 => ["mollie_ooff", "String"],
    ]);
    while ($dao->fetch()) {
      switch ($dao->name) {
        case "EFT":
          $definition->addMethodCall("setEftPaymentId", [(int) $dao->value]);
          break;
        case "mollie_frst":
          $definition->addMethodCall("setFrstPaymentId", [(int) $dao->value]);
          break;
        case "mollie_ooff":
          $definition->addMethodCall("setOoffPaymentId", [(int) $dao->value]);
          break;
        case "mollie_rcur":
          $definition->addMethodCall("setRcurPaymentId", [(int) $dao->value]);
          break;
      }
    }
  }

  /**
   * Zet property voor contributie status gereed
   *
   * @param $definition
   */
  private function setContributionStatus(&$definition) {
    $query = "SELECT cov.value, cov.name
        FROM civicrm_option_group AS cog JOIN civicrm_option_value AS cov ON cog.id = cov.option_group_id
        WHERE cog.name = %1 AND cov.name IN (%2, %3);";
    $dao = \CRM_Core_DAO::executeQuery($query, [
      1 => ["contribution_status", "String"],
      2 => ["Completed", "String"],
      3 => ["Pending", "String"],
      ]);
    while ($dao->fetch()) {
      switch ($dao->name) {
        case "Completed":
          $definition->addMethodCall('setCompletedContributionStatusId', [(int) $dao->value]);
          break;
          case "Pending";
            $definition->addMethodCall('setPendingContributionStatusId', [(int) $dao->value]);
          break;
      }
    }
  }

  /**
   * Zet properties voor financieel type donatie
   *
   * @param $definition
   */
  private function setFinancieelType(&$definition) {
    $query = "SELECT id FROM civicrm_financial_type WHERE name = %1;";
    $id = \CRM_Core_DAO::singleValueQuery($query, [1 => ["Contributie", "String"]]);
    if ($id) {
      $definition->addMethodCall('setFinancialTypeId', [(int) $id]);
    }
  }

  /**
   * Zet properties voor geslacht
   *
   * @param $definition
   */
  private function setGeslacht(&$definition) {
    $query = "SELECT cov.value, cov.name
        FROM civicrm_option_group AS cog JOIN civicrm_option_value AS cov ON cog.id = cov.option_group_id
        WHERE cog.name = %1;";
    $dao = \CRM_Core_DAO::executeQuery($query, [1 => ["gender", "String"]]);
    while ($dao->fetch()) {
      switch ($dao->name) {
        case "Female":
          $definition->addMethodCall('setVrouwGeslacht', [(int) $dao->value]);
          break;
        case "Male":
          $definition->addMethodCall('setManGeslacht', [(int) $dao->value]);
          break;
        case "Other":
          $definition->addMethodCall('setOnbekendGeslacht', [(int) $dao->value]);
          break;
      }
    }
  }

  /**
   * Zet properties voor aanhef
   *
   * @param $definition
   */
  private function setAanhef(&$definition) {
    $query = "SELECT cov.value, cov.name
        FROM civicrm_option_group AS cog JOIN civicrm_option_value AS cov ON cog.id = cov.option_group_id
        WHERE cog.name = %1 AND cov.name IN(%2, %3);";
    $dao = \CRM_Core_DAO::executeQuery($query, [
      1 => ["individual_prefix", "String"],
      2 => ["Mrs.", "String"],
      3 => ["Mr.", "String"],
    ]);
    while ($dao->fetch()) {
      switch ($dao->name) {
        case "Dhr":
        case "Mr.":
          $definition->addMethodCall('setMijnheerAanhef', [(int) $dao->value]);
          break;
        case "Mrs.":
          $definition->addMethodCall('setMevrouwAanhef', [(int) $dao->value]);
          break;
      }
    }
  }

}

